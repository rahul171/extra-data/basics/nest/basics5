import {
  Controller,
  Get,
  HttpException,
  HttpStatus,
  UseFilters,
} from '@nestjs/common';
import { Module2Service } from './module2.service';
import { ForbiddenException } from '../common/exception/handlers/forbidden.exception';
import { UnauthorizedException } from '../common/exception/handlers/unauthorized.exception';
import { CustomExceptionFilter } from '../common/exception/filters/custom-exception.filter';

@Controller('module2')
@UseFilters(CustomExceptionFilter)
export class Module2Controller {
  constructor(private module2Service: Module2Service) {}

  @Get()
  getMessage(): string {
    return this.module2Service.getMessage();
  }

  @Get('error1')
  error1(): string {
    throw new ForbiddenException();
    return 'error1';
  }

  @Get('error2')
  error2(): string {
    throw new UnauthorizedException();
    return 'error1';
  }

  @Get('error3')
  error3(): string {
    throw new HttpException('hello there', HttpStatus.CREATED);
    return 'error3';
  }
}
