import { Module } from '@nestjs/common';
import { AppService } from './app.service';
import { AppController } from './app.controller';
import { Module1Module } from './module1/module1.module';
import { Module2Module } from './module2/module2.module';
import { Module3Module } from './module3/module3.module';
import { APP_FILTER } from '@nestjs/core';
import { CustomExceptionFilter } from './common/exception/filters/custom-exception.filter';
import { Custom2ExceptionFilter } from './common/exception/filters/custom2-exception.filter';

@Module({
  imports: [Module1Module, Module2Module, Module3Module],
  providers: [
    AppService,
    // it also works without this.
    // module3 is able to use CustomExceptionFilter without the below provider.
    // comment the below provider and try it out.
    // {
    //   provide: APP_FILTER,
    //   useClass: CustomExceptionFilter,
    // },
  ],
  controllers: [AppController],
})
export class AppModule {
  constructor() {
    // setTimeout(() => {
    //   // this won't be caught by the global exception filter, because its not
    //   // an exception occurred during processing a request.
    //   throw new Error('hello new error');
    // }, 2000);
  }
}
