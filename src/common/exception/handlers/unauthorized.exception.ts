import { HttpException, HttpStatus } from '@nestjs/common';

export class UnauthorizedException extends HttpException {
  constructor() {
    super('Unauthorized exception handler message', HttpStatus.UNAUTHORIZED);
  }
}
