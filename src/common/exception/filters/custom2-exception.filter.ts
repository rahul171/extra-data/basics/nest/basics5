import {
  ArgumentsHost,
  Catch,
  HttpException,
  HttpStatus,
} from '@nestjs/common';
import { BaseExceptionFilter } from '@nestjs/core';

@Catch()
export class Custom2ExceptionFilter extends BaseExceptionFilter {
  catch(exception: unknown, host: ArgumentsHost) {
    console.log('Custom2ExceptionFilter => catch()');

    const ctx = host.switchToHttp();
    const request = ctx.getRequest();

    let newException = exception;

    // comment the if block for the normal behaviour.
    if (exception instanceof Error) {
      newException = new HttpException(
        {
          error: exception.message,
          'instanceof Error': true,
          route: request.url,
          filterName: 'Custom2ExceptionFilter',
        },
        HttpStatus.INTERNAL_SERVER_ERROR,
      );

      const next = ctx.getNext();
      // this will invoke CustomExceptionFilter.
      // next(newException);
      return next(newException);
    }

    // if the next() is called in the above if block,
    // then this will throw the error "can't send header again"
    // because CustomExceptionFilter already sent the response.
    // what to do? maybe add return statement in front of next() =>
    // return next(newException)
    // in the above if block.
    super.catch(newException, host);
  }
}
