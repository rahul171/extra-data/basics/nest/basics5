import {
  ArgumentsHost,
  Catch,
  ExceptionFilter,
  HttpException,
} from '@nestjs/common';

@Catch(HttpException)
export class CustomExceptionFilter implements ExceptionFilter {
  catch(exception: HttpException, host: ArgumentsHost) {
    console.log('CustomExceptionFilter => catch()');

    const ctx = host.switchToHttp();
    const response = ctx.getResponse();

    const exceptionObj = {
      'getStatus()': exception.getStatus(),
      'getResponse()': exception.getResponse(),
      'initMessage()': exception.initMessage(),
      message: exception.message,
      name: exception.name,
      stack: exception.stack,
      filterName: 'CustomExceptionFilter',
    };

    response.status(400).json({ exception: exceptionObj });

    // This is equivalent to throwing a new exception.
    // const next = ctx.getNext();
    // next(exception);
  }
}
