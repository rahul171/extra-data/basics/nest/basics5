import {
  ArgumentsHost,
  Catch,
  ExceptionFilter,
  HttpException,
  HttpStatus,
} from '@nestjs/common';

@Catch()
export class CustomCp1ExceptionFilter implements ExceptionFilter {
  catch(exception: HttpException, host: ArgumentsHost) {
    console.log('CustomCp1ExceptionFilter => catch()');

    const ctx = host.switchToHttp();
    const request = ctx.getRequest();
    const response = ctx.getResponse();

    // if this global exception filter is going to catch all the errors, then
    // there is no way any other global exception filter is getting called,
    // unless you provide it on controller-scoped or request-scoped.
    // in which case, the lower scoped (request scoped first) exception filter
    // will get called first, then the next lower one and so on.
    // global will get called last.
    // so there might be an error that "can't rend the response header again".
    // because response has already been sent by the lower scoped exception filter,
    // and now the higher scoped exception filter is trying to send the response
    // again.

    console.log(exception.message);

    if (exception.message.includes('cef')) {
      const next = ctx.getNext();
      return next(
        new HttpException(
          {
            from: 'CustomCp1ExceptionFilter',
          },
          HttpStatus.CREATED,
        ),
      );
    }

    const exceptionObj = {
      message: exception.message,
      filterName: 'CustomCp1ExceptionFilter',
    };

    response.status(400).json({ exception: exceptionObj });
  }
}
