import {
  Controller,
  Get,
  HttpException,
  HttpStatus,
  UseFilters,
} from '@nestjs/common';
import { Module1Service } from './module1.service';
import { ForbiddenException } from '../common/exception/handlers/forbidden.exception';
import { UnauthorizedException } from '../common/exception/handlers/unauthorized.exception';
import { CustomExceptionFilter } from '../common/exception/filters/custom-exception.filter';

@Controller('module1')
export class Module1Controller {
  constructor(private module1Service: Module1Service) {}

  @Get()
  getMessage(): string {
    return this.module1Service.getMessage();
  }

  @Get('error1')
  error1(): string {
    throw new ForbiddenException();
    return 'error1';
  }

  @Get('error2')
  @UseFilters(CustomExceptionFilter)
  error2(): string {
    throw new UnauthorizedException();
    return 'error1';
  }

  @Get('error3')
  @UseFilters(CustomExceptionFilter)
  error3(): string {
    throw new HttpException('hello there', HttpStatus.CREATED);
    return 'error3';
  }
}
