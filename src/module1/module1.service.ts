import { Injectable } from '@nestjs/common';

@Injectable()
export class Module1Service {
  getMessage(): string {
    return 'welcome to the module1 service';
  }
}
