import { Injectable } from '@nestjs/common';

@Injectable()
export class Module3Service {
  getMessage(): string {
    return 'welcome to the module3 service';
  }

  error() {
    throw new Error('from module3 service');
  }
}
