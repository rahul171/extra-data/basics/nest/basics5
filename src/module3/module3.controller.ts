import {
  Controller,
  Get,
  HttpException,
  HttpStatus,
  UseFilters,
} from '@nestjs/common';
import { Module3Service } from './module3.service';
import { ForbiddenException } from '../common/exception/handlers/forbidden.exception';
import { UnauthorizedException } from '../common/exception/handlers/unauthorized.exception';
import { Custom2ExceptionFilter } from '../common/exception/filters/custom2-exception.filter';

@Controller('module3')
// apply global exception filter here, from the main.ts file.
export class Module3Controller {
  constructor(private module3Service: Module3Service) {}

  @Get()
  getMessage(): string {
    return this.module3Service.getMessage();
  }

  @Get('error1')
  error1(): string {
    throw new ForbiddenException();
    return 'error1';
  }

  @Get('error2')
  error2(): string {
    throw new UnauthorizedException();
    return 'error1';
  }

  @Get('error3')
  error3(): string {
    throw new HttpException('hello there 3', HttpStatus.CREATED);
    return 'error3';
  }

  @Get('error4')
  // this is prioritized over a global exception filter.
  // but it will be invoked, global exception filter will be called after this.
  // hence, it might throw an error that headers are already sent,
  // because Custom2ExceptionFilter send the response, and then the
  // global exception filter might try to send it again.
  // use the below route (error5) to not use the request based route and only
  // use the global route.
  @UseFilters(Custom2ExceptionFilter)
  error4(): string {
    throw new Error('handled?');
    return 'error4';
  }

  @Get('error5')
  error5(): string {
    throw new Error('from error5');
    return 'error5';
  }

  @Get('error6')
  error6(): string {
    throw new Error('cef: from error6');
    return 'error6';
  }

  @Get('error7')
  // @UseFilters(Custom2ExceptionFilter)
  error7(): string {
    this.module3Service.error();
    return 'error7';
  }
}
