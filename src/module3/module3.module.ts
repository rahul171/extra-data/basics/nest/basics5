import { Module } from '@nestjs/common';
import { Module3Service } from './module3.service';
import { Module3Controller } from './module3.controller';

@Module({
  providers: [Module3Service],
  controllers: [Module3Controller],
})
export class Module3Module {}
