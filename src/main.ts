import { HttpAdapterHost, NestFactory } from '@nestjs/core';
import { NestExpressApplication } from '@nestjs/platform-express';
import { AppModule } from './app.module';
import { CustomExceptionFilter } from './common/exception/filters/custom-exception.filter';
import { Custom2ExceptionFilter } from './common/exception/filters/custom2-exception.filter';
import { CustomCp1ExceptionFilter } from './common/exception/filters/custom-cp1-exception.filter';

const bootstrap = async () => {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);
  // test this global scoped exception filter for the module3.
  // remove this to see the effect of request scoped and controller scoped
  // exception filters in module1 and module2.
  // app.useGlobalFilters(new CustomExceptionFilter());

  const { httpAdapter } = app.get(HttpAdapterHost);
  app.useGlobalFilters(
    // need to pass httpAdapter if extending the BaseExceptionFilter.
    // this is to register the global exception filter which extends
    // the BaseExceptionFilter.
    // comment the below line to use it in the request specific scope in module3.
    new Custom2ExceptionFilter(httpAdapter),
    new CustomExceptionFilter(),
    // see the comment in CustomCp1ExceptionFilter class file for more info about
    // what is going to happen if this exception filter is going to catch all the
    // exception.
    // new CustomCp1ExceptionFilter(),
  );
  // The exception filter's order matters in the above useGlobalFilters params.
  // Nest tries to use the last filter first, then the second last and so on
  // until the exception is handled.

  await app.listen(3000);
};

bootstrap();
